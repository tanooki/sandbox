const Group = require('@tanooki/gl.cli.js/groups.helper').Group
const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper

async function create_a_sub_group() {

  let parentGroup = await GroupsHelper.getGroupDetails({group:"tanooki-workshops"})
  //console.log(parentGroup)
  
  let parent_group_id = parentGroup.get("id")

  console.log(parent_group_id)


  try {
    let myGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: "my-other-demo-group",
      subGroupName: "my-other-demo-group"
    })
    console.log("📝", myGroup)

  } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }
  
}

async function create_a_sub_sub_group() {

  let parentGroup = await GroupsHelper.getSubGroupDetails({subGroup:"tanooki-workshops/my-other-demo-group"})
  //console.log(parentGroup)
  
  let parent_group_id = parentGroup.get("id")

  console.log(parent_group_id)


  try {
    let myGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: "sam-group",
      subGroupName: "sam-group"
    })
    console.log("📝", myGroup)
    console.log("🖐️", myGroup.get("web_url"))

  } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }
  
}

//create_a_sub_group()
create_a_sub_sub_group()
