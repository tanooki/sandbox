
require('dotenv').config()

const Issue = require('@tanooki/gl.cli.js/issues.helper.js').Issue
const IssueHelper = require('@tanooki/gl.cli.js/issues.helper.js').Helper
const Label = require('@tanooki/gl.cli.js/labels.helper.js').Label
const LabelHelper = require('@tanooki/gl.cli.js/labels.helper.js').Helper

async function create_some_labels() {
  try {
    let label1 = await LabelHelper.createLabel({project:"tanooki/sandbox", name:"text", color:"yellow"})
    console.log("📝", label1)
  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
  
  try {
    let label = await LabelHelper.createLabel({project:"tanooki/sandbox", name:"hello world", color:"pink"})
    console.log("📝", label)
  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  } 

  /*
  let label2 = await LabelHelper.createLabel({project:"tanooki/sandbox", name:"data", color:"orange"})
  await LabelHelper.createLabel({project:"tanooki/sandbox", name:"record", color:"green"})
  await LabelHelper.createLabel({project:"tanooki/sandbox", name:"secret", color:"red"})
  */
}


async function create_some_issues() {
  await IssueHelper.createIssue({
    project:"tanooki/sandbox",
    title: "🖖 Hello World 🌍",
    description: `
    ### This is a test
    > Hello I'm @k33g
    - first
    - second
    - third
    `.split("\n").map(row => row.trim()).join("\n"),
    labels: ["record", "text"]
  })
}

//TODO: initialize a "db project with the appropriate labels"
async function save_some_documents() {
  await LabelHelper.createLabel({project:"tanooki/sandbox", name:"json-doc", color:"blue"})
  
  var human = {
    id: "bob_morane",
    firstName: "Bob", 
    lastName: "Morane",
    age: 42
  }
  
  await IssueHelper.createIssue({
    project:"tanooki/sandbox",
    title: human.id,
    description: JSON.stringify(human),
    labels: ["json-doc", "record"]
  })
  
  human = {
    id: "jane_doe",
    firstName: "Jane", 
    lastName: "Doe",
    age: 25
  }
  
  await IssueHelper.createIssue({
    project:"tanooki/sandbox",
    title: human.id,
    description: JSON.stringify(human),
    labels: ["json-doc", "record"]
  })
}

async function query_json_doc() {
  let issues = await IssueHelper.getProjectIssues({project:"tanooki/sandbox", labels:"json-doc"})
  issues.forEach(issue => {
    console.log(issue.get("title"), "->", JSON.parse(issue.get("description")))
    console.log("author:", issue.get("author"))
  })
}

async function main() {
  let issues = await IssueHelper.getProjectIssues({project:"tanooki/sandbox"})
  issues.forEach(issue => {
    console.log("title:", issue.get("title"), "description:", issue.get("description"))
    
  })
    
}

async function create_some_issues_from_different_users() {
  await IssueHelper.createIssue({
    project:"tanooki/sandbox",
    title: "Hello from Admin",
    description: `Hello World`,
    labels: ["record", "text"]
  })
  await IssueHelper.createIssue({
    project:"tanooki/sandbox",
    title: "Hello from Swann",
    description: `Hello World`,
    labels: ["record", "text"],
    token: process.env.SWANNOU_TOKEN
  })
}


//create_some_issues()

//main()

//save_some_documents()

//query_json_doc()

create_some_issues_from_different_users()

/*
IssueHelper.getProjectIssues({project:"tanooki/sandbox", labels:"json-doc"})
  .then(issues => {
    issues.forEach(issue => {
      console.log("->", JSON.parse(issue.get("description")))
    })
  })
*/

//create_some_labels()






