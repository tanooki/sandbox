const axios = require("axios")

String.prototype.replaceAll = function(search, replacement) {
  var target = this
  return target.split(search).join(replacement)
}

function urlEncodedPath({ name }){
    let encoded = `${name
      .replaceAll("/", "%2F")
      .replaceAll(".", "%2E")
      .replaceAll("-", "%2D")
      .replaceAll("_", "%5F")
      .replaceAll(".", "%2E")}`;
    //console.log("👋", encoded);
    return encoded;
  }

/* access_level
    10 => Guest access
    20 => Reporter access
    30 => Developer access
    40 => Maintainer access
    50 => Owner access # Only valid for groups
*/
function accessLevelLabel({level}) {
  switch(level) {
    case 10:
      return "guest"
      break;
    case 20:
      return "reporter"
      break;
    case 30:
      return "developer"
      break;
    case 40:
      return "maintener"
      break;
    case 50:
      return "owner"
      break;
    default:
      return ""
  }
}

function gitlab({method, path, data}) {
  let token = process.env.GITLAB_TOKEN_ADMIN
  let gitlabUrl = process.env.GITLAB_URL || "https://gitlab.com"
  let headers = {
    "Content-Type": "application/json",
    "Private-Token": token
  }
  return axios({
    method: method,
    url: gitlabUrl + '/api/v4' + path,
    headers: headers,
    data: data !== null ? JSON.stringify(data) : null
  })
}


module.exports = {
  urlEncodedPath: urlEncodedPath,
  accessLevelLabel: accessLevelLabel,
  gitlab: gitlab
}

