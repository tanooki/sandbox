import {gitlab} from './core/gitlab.helper'

async function descendantGroupsOfGroup({parentGroupId, perPage, page}) {
  return gitlab({method:"GET", path:`/groups/${parentGroupId}/descendant_groups/?per_page=${perPage}&page=${page}`, data: null})
}
