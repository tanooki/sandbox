

## Issues

https://docs.gitlab.com/ee/api/issues.html#new-issue


curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.example.com/api/v4/projects/4/issues?title=Issues%20with%20auth&labels=bug"


```bash
PROJECT_ID=24581427
curl --request POST \
     --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --header "content-type: application/json" \
     --url "https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues?title=hello" \
     --data '{
        "description": "hello 👋"
     }'
```

```bash
PROJECT_ID=24581427
curl --request POST \
     --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --header "content-type: application/json" \
     --url "https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues" \
     --data '{
        "title": "🐼😃",
        "description": "hello 👋"
     }'
```

```bash
PROJECT_ID=24581427
curl --request POST \
     --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" \
     --header "content-type: application/json" \
     --url "https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues" \
     --data '{
        "title": "🐼",
        "description": "hello 👋 <hr> ![k33g](/uploads/44825d4aa74693f5027f1ba545eae573/k33g.png)"
     }'
```

## Upload

https://docs.gitlab.com/ce/api/projects.html#upload-a-file

POST /projects/:id/uploads

```bash
PROJECT_ID=24581427
curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN_ADMIN}" --form "file=@k33g.png" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads"
```

Id of the current project:
24581427

```json
{
    "alt":"k33g",
    "url":"/uploads/44825d4aa74693f5027f1ba545eae573/k33g.png",
    "full_path":"/k33g_org/at-work/uploads/44825d4aa74693f5027f1ba545eae573/k33g.png",
    "markdown":"![k33g](/uploads/44825d4aa74693f5027f1ba545eae573/k33g.png)"
}
```




