
require('dotenv').config()

const gitlab = require('@tanooki/gl.cli.js').gitlab
const urlEncodedPath = require('@tanooki/gl.cli.js').urlEncodedPath

// GET /groups/:id
// details of a group
async function detailsOfgroup({groupId}) {
  return gitlab({method:"GET", path:`/groups/${groupId}`, data: null})
}

// =========================================
//  GitLab Helpers
// =========================================

async function get_details_of_group({name}) {
  let groupId = urlEncodedPath({name: name})
   var group = {}, errors = []

  await detailsOfgroup({groupId: groupId})
    .then(response => response.data)
    .then(data => {
      group = data
      return data
    })
    .catch(error => {
      console.log("😡", error.response.statusText, error.response.status, error.response.data)
      errors.push({
        statusText: error.response.statusText,
        status: error.response.status,
        data: error.response.data
      })
    })

  return {result: group, errors: errors}
}

// =========================================
//  Main program
// =========================================
async function batch({groupPath}) {
  let parentGroupName = groupPath
  // get the parent group
  let parentGroup = await get_details_of_group({name: parentGroupName})
  console.log(parentGroup)
}
  
if(process.env.GITLAB_TOKEN_ADMIN==undefined) {
  console.log("👋 you need to set GITLAB_TOKEN_ADMIN")
  process.exit(1)
}

batch({groupPath: "tanooki"})

